﻿using System;
using System.Diagnostics;
using System.Numerics;
using System.Text;
using Algorythm;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using STBAlgorythm = Algorythm.Algorythm;

namespace STBTest
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void ApplyH()
		{
			byte n = 0xA2;
			Assert.AreEqual(0x9B, Permutation.H[n >> 4][n & 0x0f]);
		}


		[TestMethod]
		public void EncodeBlock()
		{
			string initData = "1234567890qwerty";
			byte[] data = Encoding.Default.GetBytes(initData);
			STBAlgorythm alg = new STBAlgorythm();
			alg.SetKey("1234567890qwerty");
			byte[] decoded = alg.DecodeBlock(alg.EncodeBlock(data));
			string retvalDe = Encoding.Default.GetString(decoded);

			Assert.AreEqual(initData, retvalDe);

		}
	}
}