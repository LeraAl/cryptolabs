﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;


namespace Algorythm
{
    public class Algorythm
    {
	    private const int KEY_SIZE_IN_BYTES = 32;
	    private const int BLOCK_SIZE_IN_BYTES = 16;
	    private byte[] key;
	    private uint[] intKeys;

	    public void SetKey(string initKey)
	    {
		    switch (initKey.Length)
		    {
				case 256/8:
					this.key = Encoding.Default.GetBytes(initKey);
					Debug.WriteLine(this.key.Length);
					break;
				case 128/8:
					this.key = Encoding.Default.GetBytes(initKey + initKey);
					Debug.WriteLine(this.key.Length);
					break;
				case 192/8:
					this.key = new byte[KEY_SIZE_IN_BYTES];
					byte[] keyArray = Encoding.Default.GetBytes(initKey);
					keyArray.CopyTo(this.key, 0);
					uint sevenPartOfKey = 0;
					sevenPartOfKey += BitConverter.ToUInt32(keyArray, 0);
					sevenPartOfKey += BitConverter.ToUInt32(keyArray, 4);
					sevenPartOfKey += BitConverter.ToUInt32(keyArray, 8);
					BitConverter.GetBytes(sevenPartOfKey).CopyTo(this.key, 24);

					uint eightPartOfKey = 0;
					eightPartOfKey += BitConverter.ToUInt32(keyArray, 12);
					eightPartOfKey += BitConverter.ToUInt32(keyArray, 16);
					eightPartOfKey += BitConverter.ToUInt32(keyArray, 20);
					BitConverter.GetBytes(eightPartOfKey).CopyTo(this.key, 28);
					break;
				default:
					throw new ArgumentException($"Key length can not be {initKey.Length}");
		    }

			intKeys = new uint[KEY_SIZE_IN_BYTES / 4];
		    for (int i = 0; i < this.intKeys.Length; i++)
		    {
			    this.intKeys[i] = BitConverter.ToUInt32(this.key, i*4);
		    }
	    }

	    public uint GetKey(int n)
	    {
			return this.intKeys[(n-1) % 8];
	    }

	    public byte[] EncodeBlock(byte[] data)
	    {
			if (data.Length != 16) throw new ArgumentException($"Block must consist 16 bytes. The real length = {data.Length}.");
		    uint a, b, c, d, e;
		    a = BitConverter.ToUInt32(data, 0);
		    b = BitConverter.ToUInt32(data, 4);
		    c = BitConverter.ToUInt32(data, 8);
		    d = BitConverter.ToUInt32(data, 12);

		    for (int i = 1; i <= 8; i++)
		    {
			    b = b ^ Lambda(SquareCross(a, GetKey(7 * i  - 6)), 5);
			    c = c ^ Lambda(SquareCross(d, GetKey(7 * i - 5)), 21);
			    a = SquareMinus(a, Lambda(SquareCross(b, GetKey(7 * i - 4)), 13));
			    e = Lambda(SquareCross(SquareCross(b, c), GetKey(7 * i - 3)), 21) ^ (uint)i;

			    b = SquareCross(b, e);
			    c = SquareMinus(c, e);

			    d = SquareCross(d, Lambda(SquareCross(c, GetKey(7 * i - 2)), 13));
			    b = b ^ Lambda(SquareCross(a, GetKey(7 * i - 1)), 21);
			    c = c ^ Lambda(SquareCross(d, GetKey(7 * i)), 5);

			    Swap(ref a, ref b);
				Swap(ref c, ref d);
				Swap(ref b, ref c);
		    }

			byte[] retval = new byte[BLOCK_SIZE_IN_BYTES];
			BitConverter.GetBytes(b).CopyTo(retval, 0);
			BitConverter.GetBytes(d).CopyTo(retval, 4);
			BitConverter.GetBytes(a).CopyTo(retval, 8);
			BitConverter.GetBytes(c).CopyTo(retval, 12);
		    return retval;
	    }

	    public byte[] DecodeBlock(byte[] data)
	    {
		    if (data.Length != 16) throw new ArgumentException($"Block must consist 16 bytes. The real length = {data.Length}.");
		    uint a, b, c, d, e;
		    a = BitConverter.ToUInt32(data, 0);
		    b = BitConverter.ToUInt32(data, 4);
		    c = BitConverter.ToUInt32(data, 8);
		    d = BitConverter.ToUInt32(data, 12);

		    for (int i = 8; i >= 1; i--)
		    {
			    b = b ^ Lambda(SquareCross(a, GetKey(7 * i)), 5);
			    c = c ^ Lambda(SquareCross(d, GetKey(7 * i - 1)), 21);
			    a = SquareMinus(a, Lambda(SquareCross(b, GetKey(7 * i - 2)), 13));
			    e = Lambda(SquareCross(SquareCross(b, c), GetKey(7 * i - 3)), 21) ^ (uint)i;

			    b = SquareCross(b, e);
			    c = SquareMinus(c, e);

			    d = SquareCross(d, Lambda(SquareCross(c, GetKey(7 * i - 4)), 13));
			    b = b ^ Lambda(SquareCross(a, GetKey(7 * i - 5)), 21);
			    c = c ^ Lambda(SquareCross(d, GetKey(7 * i - 6)), 5);

			    Swap(ref a, ref b);
			    Swap(ref c, ref d);
			    Swap(ref a, ref d);
		    }

		    byte[] retval = new byte[BLOCK_SIZE_IN_BYTES];
		    BitConverter.GetBytes(c).CopyTo(retval, 0);
		    BitConverter.GetBytes(a).CopyTo(retval, 4);
		    BitConverter.GetBytes(d).CopyTo(retval, 8);
		    BitConverter.GetBytes(b).CopyTo(retval, 12);
		    return retval;
	    }

		private byte ApplyH(byte n)
	    {
		    return Permutation.H[n >> 4][n & 0x0f];
	    }

	    private uint Lambda(uint n, int r)
	    {
		    byte[] nInBytes = BitConverter.GetBytes(n);
		    for (int i = 0; i < nInBytes.Length; i++)
		    {
			    nInBytes[i] = ApplyH(nInBytes[i]);
		    }

		    uint retval = Lambda(BitConverter.ToUInt32(nInBytes, 0));
		    for (int i = 1; i < r; i++)
		    {
			    retval = Lambda(retval);
		    }

		    return retval;
	    }

	    private uint Lambda(uint n)
	    {
		    uint temp = n >> 31 == 0 ? 2 * n : 2 * n + 1;

			return temp;
	    }

	    private uint SquareCross(uint a, uint b)
	    {
		    uint temp = a + b;
		    return temp;
	    }

	    private uint SquareMinus(uint a, uint b)
	    {
		    uint temp = a - b;
		    return temp;
		}

	    private void Swap(ref uint a, ref uint b)
	    {
		    uint temp = a;
		    a = b;
		    b = temp;
	    }
    }
}
