﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;


namespace Algorythm
{
    public class Algorythm
    {
	    private const int KEY_SIZE_IN_BYTES = 32;
	    private const int BLOCK_SIZE_IN_BYTES = 16;
	    private string[] keyArray;

	    public void SetKey(string initKey)
	    {
		    string key = "";
		    foreach (char c in initKey)
		    {
			    key += Convert.ToString((byte)c, 2).PadLeft(8, '0');
		    }
			keyArray = new string[8];
		    for (int i = 0; i < key.Length / 32; i++)
		    {
			    this.keyArray[i] = key.Substring(i * 32, 32);
		    }
		    switch (key.Length)
		    {
				case 128:
					Array.Copy(this.keyArray, 0, this.keyArray, 4, 4);
					break;
				case 192:
					this.keyArray[6] = Xor(keyArray[0], Xor(keyArray[1], keyArray[2]));
					this.keyArray[7] = Xor(keyArray[3], Xor(keyArray[4], keyArray[5]));
					break;
				default:
					throw new ArgumentException($"Key length can not be {initKey.Length}");
		    }

		    foreach (var s in keyArray)
		    {
			    Debug.WriteLine($"{s}");
		    }
	    }

	    public string GetKey(int n)
	    {
			return this.keyArray[(n-1) % 8];
	    }

	    public string EncodeBlock(string data)
	    {
			if (data.Length != 16*8) throw new ArgumentException($"Block must consist 16*8 chars. The real length = {data.Length}.");
		    var a = data.Substring(0, 32);
		    var b = data.Substring(32, 32);
			var c = data.Substring(64, 32);
			var d = data.Substring(96, 32);

			for (int i = 1; i <= 8; i++)
		    {
			    b = Xor(b, G(Plus(a, GetKey(7 * i  - 6)), 5));
			    c = Xor(c, G(Plus(d, GetKey(7 * i - 5)), 21));
			    a = Minus(a, G(Plus(b, GetKey(7 * i - 4)), 13));
			    var e = Xor(G(Plus(Plus(b, c), GetKey(7 * i - 3)), 21), U((uint)i));

			    b = Plus(b, e);
			    c = Minus(c, e);

			    d = Plus(d, G(Plus(c, GetKey(7 * i - 2)), 13));
			    b = Xor(b, G(Plus(a, GetKey(7 * i - 1)), 21));
			    c = Xor(c, G(Plus(d, GetKey(7 * i)), 5));

			    Swap(ref a, ref b);
				Swap(ref c, ref d);
				Swap(ref b, ref c);
		    }

		    return string.Join("", b, d, a, c);
	    }

	    public string DecodeBlock(string data)
	    {
		    if (data.Length != 16*8) throw new ArgumentException($"Block must consist 16*8. The real length = {data.Length}.");
		    string a, b, c, d, e;
			a = data.Substring(0, 32);
		    b = data.Substring(32, 32);
		    c = data.Substring(64, 32);
		    d = data.Substring(96, 32);

			for (int i = 8; i >= 1; i--)
		    {
			    b = Xor(b, G(Plus(a, GetKey(7 * i)), 5));
			    c = Xor(c, G(Plus(d, GetKey(7 * i - 1)), 21));
			    a = Minus(a, G(Plus(b, GetKey(7 * i - 2)), 13));
			    e = Xor(G(Plus(Plus(b, c), GetKey(7 * i - 3)), 21), U((uint)i));

			    b = Plus(b, e);
			    c = Minus(c, e);

			    d = Plus(d, G(Plus(c, GetKey(7 * i - 4)), 13));
			    b = Xor(b, G(Plus(a, GetKey(7 * i - 5)), 21));
			    c = Xor(c, G(Plus(d, GetKey(7 * i - 6)), 5));

			    Swap(ref a, ref b);
			    Swap(ref c, ref d);
			    Swap(ref a, ref d);
		    }
		    return string.Join("", c, a, d, b);
	    }

		private string ApplyH(string s)
		{
			int n = Convert.ToInt32(s, 2);
		    return Convert.ToString(Permutation.H[n >> 4][n & 0x0f], 2).PadLeft(8, '0');
	    }

	    private string G(string s, int r)
	    {
			Debug.WriteLine($"G{r}: s: {s}");
		    string newS = "";
		    for (int i = 0; i < s.Length/8; i++)
		    {
			    newS += ApplyH(s.Substring(i*8, 8));
		    }
		    Debug.WriteLine($"G{r}: newS: {newS}");
			return Lambda(newS, r);
	    }

	    private string Lambda(string s, int r)
		{ 
		    for (int i = 0; i < r; i++)
		    {
			    s = Lambda(s);
		    }

		    return s;
	    }

	    private string Lambda(string n)
	    {
		    uint number = u_(n);
//			Debug.WriteLine($"Lambda: {number}");
		    if (number < (uint) Math.Pow(2, 31))
			    return U(2 * number, 32);
		    else
			    return U(2 * number + 1, 32);
	    }

	    private string Plus(string a, string b, int power=32)
	    {
			Debug.WriteLine($"plus: {a} {b}");
			Debug.WriteLine($"plus: {U(u_(a) + u_(b), power)}");
		    return U(u_(a) + u_(b), power);
	    }

	    private string Minus(string a, string b, int power=32)
	    {
			return U(u_(a) - u_(b), power);
		}

		private uint u_(string str)
	    {
		    return Convert.ToUInt32(str, 2);
		}

	    private string U(uint n, int power=32)
	    {
		    return Convert.ToString(n, 2).PadLeft(power, '0');
	    }

	    private void Swap(ref string a, ref string b)
	    {
		    string temp = a;
		    a = b;
		    b = temp;
	    }

	    private string Xor(string a, string b)
	    {
			if (a.Length != b.Length) throw new ArgumentException();

		    string retval = "";
		    for (int i = 0; i < a.Length; i++)
		    {
			    retval += a[i] == b[i] ? "0" : "1";
		    }

		    return retval;
	    }
    }
}
