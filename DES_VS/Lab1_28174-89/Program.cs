using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms;

namespace LabConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ApplyGOST();
            ApplyDES();

            Console.ReadKey();
        }

        private static void ApplyGOST()
        {
            GOST_Algorythm gostAlg = new GOST_Algorythm();

            string path = "../../input.txt";
            string pathEncoded = "../../output_encoded_gost.txt";
            string pathDecoded = "../../output_decoded_gost.txt";

            string key = "1234567890111213";

            gostAlg.SetKey(key);

            try
            {
                Console.WriteLine("GOST 28174-89.");
                Console.WriteLine("Start encoding.");

                byte[] encoded = gostAlg.Encode(File.ReadAllBytes(path));

                Console.WriteLine("Output file: " + pathEncoded + "\nStart decoding.");

                byte[] decoded = gostAlg.Decode(encoded);

                Console.WriteLine("Output file: " + pathDecoded + "\nEnd.");
                File.WriteAllBytes(pathEncoded, encoded);
                File.WriteAllBytes(pathDecoded, decoded);
            }
            catch (IOException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private static void ApplyDES()
        {
            DESAlgorithm destAlg = new DESAlgorithm();

            string path = "../../input.txt";
            string pathDoubleEncoded = "../../output_encoded_double_des.txt";
            string pathDoubleDecoded = "../../output_decoded_double_des.txt";

            string pathTripleEncoded = "../../output_encoded_triple_des.txt";
            string pathTripleDecoded = "../../output_decoded_triple_des.txt";

            string firstKey = "12345678";
            string secondKey = "90111213";

            try
            {

                #region Double DES

                Console.WriteLine("Double DES.");
                Console.WriteLine("Start encoding.");

                destAlg.SetKey(firstKey);
                byte[] encoded = destAlg.Encode(File.ReadAllBytes(path));
                destAlg.SetKey(secondKey);
                encoded = destAlg.Encode(encoded);

                Console.WriteLine("Output file: " + pathDoubleEncoded + "\nStart decoding.");

                byte[] decoded = destAlg.Decode(encoded);
                destAlg.SetKey(firstKey);
                decoded = destAlg.Decode(decoded);

                Console.WriteLine("Output file: " + pathDoubleDecoded + "\nEnd.");
                File.WriteAllBytes(pathDoubleEncoded, encoded);
                File.WriteAllBytes(pathDoubleDecoded, decoded);

                #endregion


                #region Triple DES 

                Console.WriteLine("Triple DES.");
                Console.WriteLine("Start encoding.");

                destAlg.SetKey(firstKey);
                encoded = destAlg.Encode(File.ReadAllBytes(path));
                destAlg.SetKey(secondKey);
                encoded = destAlg.Decode(encoded, false);
                destAlg.SetKey(firstKey);
                encoded = destAlg.Encode(encoded);

                Console.WriteLine("Output file: " + pathTripleEncoded + "\nStart decoding.");

                decoded = destAlg.Decode(encoded);
                destAlg.SetKey(secondKey);
                decoded = destAlg.Encode(decoded, false);
                destAlg.SetKey(firstKey);
                decoded = destAlg.Decode(decoded);

                Console.WriteLine("Output file: " + pathTripleDecoded + "\nEnd.");
                File.WriteAllBytes(pathTripleEncoded, encoded);
                File.WriteAllBytes(pathTripleDecoded, decoded);

                #endregion

            }
            catch (IOException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
