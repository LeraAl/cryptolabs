﻿using Algorithms;
using NUnit.Framework;

namespace Lab1_28174_89
{
    [TestFixture]
    class AlgorithmTest
    {
        private GOST_Algorythm algorithm = new GOST_Algorythm();

        [TestCase(0)]
        [TestCase(8)]
        [TestCase(16)]
        [TestCase(31)]
        public void GetKeyEncrypt_Return0(int i)
        {
            Assert.AreEqual(0, algorithm.GetKeyIndex(i, true));
        }


        [TestCase(4)]
        [TestCase(12)]
        [TestCase(20)]
        [TestCase(27)]
        public void GetKeyEncrypt_Return4(int i)
        {
            Assert.AreEqual(4, algorithm.GetKeyIndex(i, true));
        }


        [TestCase(0)]
        [TestCase(15)]
        [TestCase(23)]
        [TestCase(31)]
        public void GetKeyDecrypt_Return0(int i)
        {
            Assert.AreEqual(0, algorithm.GetKeyIndex(i, false));
        }


        [TestCase(4)]
        [TestCase(11)]
        [TestCase(19)]
        [TestCase(27)]
        public void GetKeyDecrypt_Return4(int i)
        {
            Assert.AreEqual(4, algorithm.GetKeyIndex(i, false));
        }
    }
}
