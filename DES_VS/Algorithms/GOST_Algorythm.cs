﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithms
{
    public class GOST_Algorythm
    {
        private const int KEYSIZE = 16; // chars
        private const int BLOCKSIZE = 8; // in bytes

        private uint[] _keys;

        public byte[] Encode(byte[] data)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));

            var i = 0;
            var decodedData = new List<byte>();

            while (i <= data.Length - BLOCKSIZE)
            {
                decodedData.AddRange(TransformBlock(data.Skip(i).Take(BLOCKSIZE).ToArray(), true));
                i += BLOCKSIZE;
            }

            if (i == data.Length)
            {
                var endBlock = new byte[BLOCKSIZE];
                endBlock[BLOCKSIZE - 1] = BLOCKSIZE;
                decodedData.AddRange(TransformBlock(endBlock, true));
            }
            else
            {
                var endBlock = new byte[BLOCKSIZE];
                Array.Copy(data, i, endBlock, 0, data.Length - i);
                endBlock[BLOCKSIZE - 1] = (byte) (BLOCKSIZE - (data.Length - i));
                decodedData.AddRange(TransformBlock(endBlock, true));
            }

            return decodedData.ToArray();
        }

        public byte[] Decode(byte[] data)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));

            var i = 0;
            var encodedData = new List<byte>();

            while (i < data.Length)
            {
                encodedData.AddRange(TransformBlock(data.Skip(i).Take(BLOCKSIZE).ToArray(), false));
                i += BLOCKSIZE;
            }

            encodedData.RemoveRange(data.Length - encodedData.Last(), encodedData.Last());

            return encodedData.ToArray();
        }

        public byte[] TransformBlock(byte[] data, bool isEncoding)
        {
            var binaryData = BitConverter.ToUInt64(data, 0);

            var L = (uint) (binaryData >> 32);
            var R = (uint) binaryData;

            for (var i = 0; i < 32; i++)
            {
                var temp = R;
                R = L ^ ApplyFunctionF(R, L, i, isEncoding);
                L = temp;
            }

            var retval = BitConverter.GetBytes(((ulong) R << 32) + L);
            return retval;
        }

        private uint ApplyFunctionF(uint r, uint l, int iteration, bool isEncryption)
        {
            var module = uint.MaxValue;
            var key = _keys[GetKeyIndex(iteration, isEncryption)];
            var n = (r + key) % module;

            var t = Substitution(n);

            var retval = (t << 11) | (t >> (32 - 11));
            return retval;
        }

        protected uint Substitution(uint value)
        {
            uint output = 0;

            for (var i = 7; i >= 0; i--)
            {
                var temp = (byte)((value >> (4 * i)) & 0x0f); // get last 4 bits
                temp = GOST_ermutations.S[i][temp];
                output |= (uint)temp << (4 * i);
            }

            return output;
        }

        public void SetKey(string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (key.Length != KEYSIZE) throw new ArgumentException("", nameof(key));

            _keys = new uint[8];
            for (var i = 0; i < 8; i++)
            {
                _keys[i] += (uint) key[i * 2] << 16;
                _keys[i] += key[i * 2 + 1];
            }
        }

        public int GetKeyIndex(int i, bool encrypt)
        {
            if (encrypt)
                return i < 24 ? i % 8 : 7 - i % 8;
            return i < 8 ? i : 7 - i % 8;
        }
    }
}