﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Algorithms
{
    public class DESAlgorithm
    {
        private const int KEYSIZE_DES = 8;
        private const int BLOCKSIZE_DES = 8;

        private ulong[] _keys = null;

        public byte[] Encode(byte[] data, bool addEmptyBlock = true)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            int i = 0;
            List<byte> decodedData = new List<byte>();

            while (i <= data.Length - BLOCKSIZE_DES)
            {
                decodedData.AddRange(EncodeBlock(data.Skip(i).Take(BLOCKSIZE_DES).ToArray()));
                i += BLOCKSIZE_DES;
            }

            if (i == data.Length)
            {
                if(addEmptyBlock)
                    decodedData.AddRange(EncodeBlock(new byte[] { 0, 0, 0, 0, 0, 0, 0, 8 }));
            }
            else
            {
                byte[] arr = new byte[BLOCKSIZE_DES];
                Array.Copy(data, i, arr, 0, data.Length - i);
                arr[BLOCKSIZE_DES - 1] = (byte)(BLOCKSIZE_DES - (data.Length - i));
                decodedData.AddRange(EncodeBlock(arr));
            }

            return decodedData.ToArray();
        }

        public byte[] Decode(byte[] data, bool deleteLast = true)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            int i = 0;
            List<byte> encodedData = new List<byte>();

            while (i < data.Length)
            {
                encodedData.AddRange(DecodeBlock(data.Skip(i).Take(BLOCKSIZE_DES).ToArray()));
                i += BLOCKSIZE_DES;
            }

            if(deleteLast)
                encodedData.RemoveRange(data.Length - encodedData.Last(), encodedData.Last());

            return encodedData.ToArray();
        }

        public byte[] EncodeBlock(byte[] data)
        {
            ulong binaryData = 0;
            data.Select(i => binaryData = ((binaryData << 8) + i)).ToList();


            ulong IPBinaryData = ApplyInitialPermutation(binaryData);

            Debug.WriteLine("binaryData: {0}", (object)Convert.ToString((long)binaryData, 2).PadLeft(64, '0'));
            Debug.WriteLine("IPBinaryData: {0}", (object)Convert.ToString((long)IPBinaryData, 2).PadLeft(64, '0'));

            uint L = (uint)(IPBinaryData >> 32);
            uint R = (uint)IPBinaryData;

            Debug.WriteLine("L: {0}", (object)Convert.ToString(L, 2).PadLeft(32, '0'));
            Debug.WriteLine("R: {0}", (object)Convert.ToString(R, 2).PadLeft(32, '0'));

            for (int i = 0; i < 16; i++)
            {
                uint temp = R;
                R = L ^ ApplyFunctionF(R, i);
                L = temp;
                Debug.WriteLine("L{1}: {0}", (object)Convert.ToString(L, 2).PadLeft(32, '0'), i + 1);
                Debug.WriteLine("R{1}: {0}", (object)Convert.ToString(R, 2).PadLeft(32, '0'), i + 1);
            }

            byte[] retval = new byte[8];
            BitConverter.GetBytes(ApplyFinalPermutation((((ulong)R) << 32) + L)).Reverse().ToArray().CopyTo(retval, 0);
            Debug.WriteLine("Final: {0}", (object)Convert.ToString((long)ApplyFinalPermutation((((ulong)R) << 32) + L), 2).PadLeft(64, '0'));
            return retval;
        }

        public byte[] DecodeBlock(byte[] data)
        {

            ulong binaryData = 0;
            data.Select(i => binaryData = ((binaryData << 8) + i)).ToList();

            ulong IP1BinaryData = ApplyInitialPermutation(binaryData);

            Debug.WriteLine("binaryData: {0}", (object)Convert.ToString((long)binaryData, 2).PadLeft(64, '0'));
            Debug.WriteLine("IPBinaryData: {0}", (object)Convert.ToString((long)IP1BinaryData, 2).PadLeft(64, '0'));


            uint R = (uint)(IP1BinaryData >> 32);
            uint L = (uint)IP1BinaryData;

            Debug.WriteLine("L: {0}", (object)Convert.ToString(L, 2));
            Debug.WriteLine("R: {0}", (object)Convert.ToString(R, 2));

            for (int i = 16; i > 0; i--)
            {
                uint temp = L;
                L = R ^ ApplyFunctionF(L, i - 1);
                R = temp;
                Debug.WriteLine("L{1}: {0}", (object)Convert.ToString(L, 2).PadLeft(32, '0'), i);
                Debug.WriteLine("R{1}: {0}", (object)Convert.ToString(R, 2).PadLeft(32, '0'), i);
            }

            byte[] retval = new byte[8];
            BitConverter.GetBytes(ApplyFinalPermutation((((ulong)L) << 32) + R)).Reverse().ToArray().CopyTo(retval, 0);
            Debug.WriteLine("Final: {0}", (object)Convert.ToString((long)ApplyFinalPermutation((((ulong)L) << 32) + R), 2).PadLeft(64, '0'));
            return retval;
        }

        private ulong ApplyInitialPermutation(ulong lr)
        {
            string LRString = Convert.ToString((long)lr, 2).PadLeft(64, '0');
            ulong retval = 0;
            for (int i = 0; i < 64; i++)
            {
                retval = retval << 1;
                if (LRString[DESPermutations.InitialPermutation[i] - 1] == '1')
                    retval += 1;
            }
            return retval;
        }

        private uint ApplyFunctionF(uint r, int iteration)
        {
            ulong KR = ApplyPermutationE(r) ^ _keys[iteration];
            string B = Convert.ToString((long)KR, 2).PadLeft(48, '0');
            string B2 = "";
            for (int i = 0; i < 8; i++)
            {
                B2 += ApplyTransformationS(B.Substring(i * 6, 6), i);
            }
            uint retval = 0;
            for (int i = 0; i < 32; i++)
            {
                retval = retval << 1;
                if (B2[DESPermutations.PPermutation[i] - 1] == '1')
                    retval += 1;
            }
            return retval;
        }

        private ulong ApplyPermutationE(uint r)
        {
            string binaryR = Convert.ToString((long)r, 2).PadLeft(32, '0');

            ulong extendedR = 0;
            for (int i = 0; i < 48; i++)
            {
                extendedR = extendedR << 1;
                if (binaryR[DESPermutations.EPermutation[i] - 1] == '1')
                    extendedR += 1;
            }

            return extendedR;
        }

        private string ApplyTransformationS(string b, int iteration)
        {
            int row = Convert.ToInt32(String.Join("", b[0], b[5]), 2);
            int col = Convert.ToInt32(b.Substring(1, 4), 2);
            return Convert.ToString(DESPermutations.SPermutation[iteration][row * 16 + col], 2).PadLeft(4, '0');
        }

        private ulong ApplyFinalPermutation(ulong lr)
        {
            string LRString = Convert.ToString((long)lr, 2).PadLeft(64, '0');
            Debug.WriteLine("LR: {0}", (object)LRString);
            ulong retval = 0;
            for (int i = 0; i < 64; i++)
            {
                retval = retval << 1;
                if (LRString[DESPermutations.FinalPermutation[i] - 1] == '1')
                    retval += 1;
            }
            return retval;
        }

        public void SetKey(string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (key.Length != KEYSIZE_DES) throw new ArgumentException("", nameof(key));

            _keys = new ulong[16];
            string binaryKey = String.Join("", key.Select(i => Convert.ToString(i, 2).PadLeft(8, '0')));

            uint C = 0, D = 0;

            // Generate C0, D0
            for (int i = 0; i < 28; i++)
            {
                C = C << 1;
                D = D << 1;

                if (binaryKey[DESPermutations.CPermutation[i] - 1] == '1')
                    C += 1;
                if (binaryKey[DESPermutations.DPermutation[i] - 1] == '1')
                    D += 1;
            }

            // Shift CD and generate keys
            for (int i = 0; i < 16; i++)
            {
                int shift = DESPermutations.LeftShift[i];
                C = 0x0FFFFFFF & ((C << shift) | (C >> (28 - shift)));
                D = 0x0FFFFFFF & ((D << shift) | (D >> (28 - shift)));

                string CD = Convert.ToString(C, 2).PadLeft(28, '0')
                            + Convert.ToString(D, 2).PadLeft(28, '0');

                ulong temp = 0;
                for (int j = 0; j < 48; j++)
                {
                    temp = temp << 1;
                    if (CD[DESPermutations.KeyPermutation[j] - 1] == '1')
                    {
                        temp++;
                    }
                }
                Debug.WriteLine("K{0}: {1}", i + 1, Convert.ToString((long)temp, 2));
                _keys[i] = temp;
            }
        }
    }
}
