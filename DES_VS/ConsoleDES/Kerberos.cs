﻿using System;
using System.Text;
using Algorithms;

namespace Kerberos
{
    public class KerberosServer
    {
        private const string ClientID_Kerberos = "client";
        private const string ClientKey_Kerberos = "qwerty00";

        private const string TGSID_Kerberos = "tgsId";

        private const string ClientToTGSKey_Kerberos = "cltotgsk";
        private const string ASToTGSKey_Kerberos = "astotgsk";

        private const string Period_Kerberos = "48";

        public Tuple<string, string> GetTGT(string clientID)
        {
            if (clientID == null || clientID != ClientID_Kerberos)
            {
                return null;
            }

            DESAlgorithm decryptor = new DESAlgorithm();
            decryptor.SetKey(ASToTGSKey_Kerberos);

            string TGT = String.Join(";", ClientID_Kerberos, TGSID_Kerberos, DateTime.Now.ToString(), Period_Kerberos, ClientToTGSKey_Kerberos);
            string decryptedTGT = Encoding.Default.GetString(decryptor.Encode(Encoding.Default.GetBytes(TGT)));

            decryptor.SetKey(ClientKey_Kerberos);
            string decryptedKey = Encoding.Default.GetString(decryptor.Encode(Encoding.Default.GetBytes(ClientToTGSKey_Kerberos)));

            return new Tuple<string, string>(decryptedTGT, decryptedKey);
        }
    }

    public class TGS
    {
        private const string ClientToTGSKey_TGS = "cltotgsk";
        private const string ASToTGSKey_TGS = "astotgsk";

        private const string Period_TGS = "2";

        private const string ClientToSSKEy_TGS = "ctosskey";
        private const string TGSToSSKey_TGS = "tgstossk";

        public Tuple<string, string> GetTGS(string tgt, string aut, string serviceID)
        {
            DESAlgorithm des = new DESAlgorithm();
            des.SetKey(ASToTGSKey_TGS);
            string[] encryptedTGT = Encoding.Default.GetString(des.Decode(Encoding.Default.GetBytes(tgt))).Split(';');

            des.SetKey(encryptedTGT[4]);
            string[] encryptedAut = Encoding.Default.GetString(des.Decode(Encoding.Default.GetBytes(aut))).Split(';');

            if (encryptedTGT[0] != encryptedAut[0])
            {
                throw new Exception("IDs of client didn't match.");
            }

            DateTime t1 = DateTime.Parse(encryptedTGT[2]);
            DateTime t2 = DateTime.Parse(encryptedAut[1]);

            if ((t2 - t1).Hours > Int32.Parse(encryptedTGT[3]))
            {
                return null;
            }

            des.SetKey(TGSToSSKey_TGS);
            string TGS_ = String.Join(";", encryptedTGT[0], serviceID, DateTime.Now.ToString(), Period_TGS, ClientToSSKEy_TGS);
            string decryptedTGS = Encoding.Default.GetString(des.Encode(Encoding.Default.GetBytes(TGS_)));

            des.SetKey(ClientToTGSKey_TGS);
            string decryptedKey = Encoding.Default.GetString(des.Encode(Encoding.Default.GetBytes(ClientToSSKEy_TGS)));

            return new Tuple<string, string>(decryptedTGS, decryptedKey);
        }
    }

    public class SS
    {
        private const string ClientToSSKey_SS = "ctosskey";
        private const string TGSToSSKey_SS = "tgstossk";

        public string Connect(string tgs, string aut)
        {
            DESAlgorithm des = new DESAlgorithm();
            des.SetKey(TGSToSSKey_SS);
            string[] encryptedTGS = Encoding.Default.GetString(des.Decode(Encoding.Default.GetBytes(tgs))).Split(';');

            des.SetKey(encryptedTGS[4]);
            string[] encryptedAut = Encoding.Default.GetString(des.Decode(Encoding.Default.GetBytes(aut))).Split(';');

            if (encryptedTGS[0] != encryptedAut[0])
            {
                throw new Exception("IDs of client didn't match.");
            }

            DateTime t1 = DateTime.Parse(encryptedTGS[2]);
            DateTime t2 = DateTime.Parse(encryptedAut[1]);

            if ((t2 - t1).Hours > Int32.Parse(encryptedTGS[3]))
            {
                return null;
            }

            string t3 = (t2 + TimeSpan.FromSeconds(1)).ToString();
            return Encoding.Default.GetString(des.Encode(Encoding.Default.GetBytes(t3)));
        }
    }


    
}
