﻿using System;
using System.Text;
using System.Linq;
namespace Kerberos
{
    class Program
    {
        static string ClientID = "client";
        static string ClientKey = "qwerty00";
        static string ServiceID = "service";

        static void Main(string[] args)
        {
            KerberosServer AS = new KerberosServer();
            TGS TGS = new TGS();
            SS ss = new SS();
            DESAlgorithm des = new DESAlgorithm();

            Console.WriteLine($"ClientID:\n{ClientID}");

            Console.WriteLine("\nGet TGT...\n");
            Tuple<string, string> TGT_KEY = AS.GetTGT(ClientID);
            Console.WriteLine("TGT:\n" +
                              $"{TGT_KEY.Item1}\n\n" +
                              "Decrypted ClientToTGSKey:\n" +
                              $"{TGT_KEY.Item2}\n");
            des.SetKey(ClientKey);
            string cLientToTGSKey = Encoding.Default.GetString(des.Encode(Encoding.Default.GetBytes(TGT_KEY.Item2)));
            Console.WriteLine($"ClientToTGSKey:\n{cLientToTGSKey}\n");


            Console.WriteLine("\nGet TGS...\n");
            des.SetKey(cLientToTGSKey);
            string aut1 = String.Join(";", ClientID, DateTime.Now.ToString());
            string decryptedAut = Encoding.Default.GetString(des.Decode(Encoding.Default.GetBytes(aut1)));
            Console.WriteLine($"Aut1:\n{aut1}\n");
            Console.WriteLine($"Decrypted Aut1:\n{decryptedAut}\n");
            Tuple<string, string> TGS_KEY = TGS.GetTGS(TGT_KEY.Item1, decryptedAut, ServiceID);
            Console.WriteLine("TGS:\n" +
                              $"{TGS_KEY.Item1}\n\n" +
                              "Decrypted ClientToSSKey:\n" +
                              $"{TGS_KEY.Item2}\n");
            string cLientToSSKey = Encoding.Default.GetString(des.Encode(Encoding.Default.GetBytes(TGS_KEY.Item2)));
            Console.WriteLine($"ClientToSSKey:\n{cLientToSSKey}\n");


            Console.WriteLine("\nConnect to server...\n");
            des.SetKey(cLientToSSKey);
            DateTime t2 = DateTime.Now;
            string aut2 = String.Join(";", ClientID, t2.ToString());
            string decryptedAut2 = Encoding.Default.GetString(des.Decode(Encoding.Default.GetBytes(aut2)));
            Console.WriteLine($"Aut2:\n{aut2}");
            Console.WriteLine($"Decrypted Aut2:\n{decryptedAut2}\n");

            string aut3 = ss.Connect(TGS_KEY.Item1, decryptedAut2);
            string decryptedAut3 = Encoding.Default.GetString(des.Encode(Encoding.Default.GetBytes(aut3)));
            Console.WriteLine($"Aut3:\n{aut3}");
            Console.WriteLine($"Encrypted Aut3:\n{decryptedAut3}\n");

            if ((DateTime.Parse(decryptedAut3) - t2).TotalSeconds <= 1)
            {
                Console.WriteLine("Connection established");
            }
            else
            {
                Console.WriteLine("Connection didn't establish");
            }


            Console.ReadKey();
        }
    }
}
